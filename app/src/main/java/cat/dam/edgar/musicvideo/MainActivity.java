package cat.dam.edgar.musicvideo;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

public class MainActivity extends AppCompatActivity
{
    private TextView tv_song;
    private Button btn_playLocal, btn_playRemote, btn_pauseMusic, btn_playMusic;
    private VideoView vv_local;
    private MediaPlayer song;
    private YouTubePlayerView yt_remoteVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setVariables();
        setListeners();
        setSongText();
        startMusic();
    }

    private void setVariables()
    {
        tv_song = (TextView) findViewById(R.id.tv_song);
        vv_local = (VideoView) findViewById(R.id.vv_local);
        yt_remoteVideo = (YouTubePlayerView) findViewById(R.id.yt_remote_video);
        setBtnVar();
        setLocalVideo();
    }

    private void setBtnVar()
    {
        btn_playLocal = (Button) findViewById(R.id.btn_play_local);
        btn_playRemote = (Button) findViewById(R.id.btn_play_remote);
        btn_pauseMusic = (Button) findViewById(R.id.btn_pause_music);
        btn_playMusic = (Button) findViewById(R.id.btn_play_music);
    }

    private void setLocalVideo()
    {
        String path = "android.resource://" + getPackageName() + "/" + R.raw.nyancat;
        MediaController mediaController = new MediaController(this);

        mediaController.setAnchorView(vv_local);
        vv_local.setMediaController(mediaController);
        vv_local.setVideoPath(path);
    }

    private void setSongText()
    {
        String songName = getResources().getResourceEntryName(R.raw.imaginedragons_natural);
        tv_song.setText(songName);
    }

    private void setListeners()
    {
        btn_playLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yt_remoteVideo.setVisibility(View.GONE);
                vv_local.setVisibility(View.VISIBLE);
                vv_local.start();
            }
        });

        btn_playRemote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vv_local.setVisibility(View.GONE);
                vv_local.stopPlayback();
                yt_remoteVideo.setVisibility(View.VISIBLE);
                remoteVideo();
            }
        });

        btn_pauseMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                song.pause();
            }
        });

        btn_playMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                song.start();
            }
        });
    }

    private void startMusic()
    {
        song = MediaPlayer.create(getApplicationContext(), R.raw.imaginedragons_natural);
        song.start();
    }

    private void remoteVideo()
    {
        String link = "B4LvDiIi128";

        getLifecycle().addObserver(yt_remoteVideo);
        yt_remoteVideo.getYouTubePlayerWhenReady(youTubePlayer -> {youTubePlayer.cueVideo(link, 0);});
    }
}